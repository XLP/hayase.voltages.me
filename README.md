# hayase.voltages.me

Source files for the Discord bot Hayase website https://hayase.voltages.me

Rewrote this completely in plain HTML, CSS. Might add more to it later on.

## Dependencies

Used for testing, building and deploying the site to Nginx.

``
    "parcel": "^2.0.1",
    "sharp": "^0.29.1"
``

## License
This is licensed under the GNU General Public License v3.0

[Click here to view it.](LICENSE)

